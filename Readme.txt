Initial setup:
--------------
- Modify appkey,appsecret,sessionkey,refreshtoken,tmallurl in config file.


Product Import to Tmall
-----------------------
-The console application can add import products with published status into Tmall
-In order to upload the picture of the products, need to create an account at picture hosting website provided by taobao


Product Update
--------------
-The console application will be able to update product stock quantity based on the stock quantity in PF 


Order Import to PF
------------------
-Firstly, it will retrieve all orders up to 3 months before today and store orderID in database
  -Only successful paid orders will be retrieved
  -Loop through database for orderID and get the details of the order
  -Import the order into PF
-For the increment orders, it will call different API 
  -The difference between start and end time can be up to 1 day

