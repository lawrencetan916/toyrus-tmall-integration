﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PFTMall
{
    class PFTmallServiceBase
    {
        Stopwatch stopWatch;
        TimeSpan timeSpan;
        string elapsedTime;

        public string serviceName { get; set; }
        public bool runThread { get; set; }
        public Thread threadHandler { get; set; }

        public int threadDelay { get; set; }
        public string processId { get; set; }

        public virtual void ExecuteTask()
        {
        }

        public virtual void ExecuteTask(Object o)
        {
        }
        protected PFTmallAPI tmallApi;
        public PFTmallAPI pfTmallApi
        {
            get
            {
                return tmallApi;
            }
        }
        protected PFDatabaseHandler databaseHandler;
        public PFDatabaseHandler DatabaseHandler
        {
            get
            {
                return databaseHandler;
            }
        }
        protected PFAPICall pfApi;
        public PFAPICall pfApiCall
        {
            get
            {
                return pfApi;
            }
        }
        protected PFErrorHandler errorHandler;
        public PFErrorHandler ErrorHandler
        {
            get
            {
                return errorHandler;
            }
        }
        public PFTmallServiceBase()
        {
            stopWatch = new Stopwatch();
            runThread = true;
            pfApi = new PFAPICall();
            this.databaseHandler = new PFDatabaseHandler();
            this.errorHandler = new PFErrorHandler();
            this.errorHandler.pfTmallServiceBase = this;
        }
        public PFTmallServiceBase(int clientId)
        {
            stopWatch = new Stopwatch();
            runThread = true;
            pfApi = new PFAPICall();
            pfApi.clientId = clientId;
            this.databaseHandler = new PFDatabaseHandler();
            pfApi.pfDatabaseHandler = DatabaseHandler;
            this.errorHandler = new PFErrorHandler();
            this.errorHandler.pfTmallServiceBase = this;
        }
        public void writeLog(string message, string errorType)
        {
            FileStream fileStream;
            string logFilePath = ConfigurationManager.AppSettings["LogFileLocation"];
            if (logFilePath != null)
            {
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }
                fileStream = new FileStream(string.Format("{0}{1}{2}.txt",
                    logFilePath, "PFTmallIntegrationLog", DateTime.Now.ToString("yyyyMMdd")),
                    FileMode.OpenOrCreate, FileAccess.Write);

            }
            else
            {
                fileStream = new FileStream(string.Format(Directory.GetCurrentDirectory() + "\\PFTmallIntegrationLog{0}.txt",
                DateTime.Now.ToString("yyyyMMdd")),
                FileMode.OpenOrCreate, FileAccess.Write);
            }
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            streamWriter.WriteLine(string.Format("Log {0} ({1}): {2} \n", errorType,
                DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"), message));
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
