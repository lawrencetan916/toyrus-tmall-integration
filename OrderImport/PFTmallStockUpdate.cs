﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Top.Api;

namespace PFTMall
{
    class PFTmallStockUpdate : PFTmallServiceBase
    {
        #region Constructor
        public PFTmallStockUpdate() : base()
        {
        }
        #endregion

        #region Public Method
        public override void ExecuteTask()
        {
            ExecuteTask(null);
        }

        public override void ExecuteTask(Object o)
        {
            Console.WriteLine("Stock Update");
            while (runThread)
            {
                this.DatabaseHandler.BeginTransaction();
                Thread.Sleep(new TimeSpan(0, 0, 1));
                try
                {
                    pfApi.LoadClientSettings();
                    tmallApi = new PFTmallAPI();
                    tmallApi.appKey = pfApi.AppKey;
                    tmallApi.appSecret = pfApi.AppSecret;
                    tmallApi.sessionKey = pfApi.SessionKey;
                    tmallApi.client = pfApi.client;
                    tmallApi.pfAPi = pfApi;
                    updateStock();
                    this.DatabaseHandler.CommitTransaction();

                }
                catch (Exception ex)
                {
                    this.DatabaseHandler.RollBackTransaction();
                    this.errorHandler.LogError(ex, pfApi.clientId, this.processId);
                }
            }
        }

        //Update stock quantity in Tmall 
        public void updateStock()
        {
            int pfStocksTotalPages;
            string tmallResponse;
            string pfResponse;
            try
            {
                writeLog("Get Total Pages of Product From PF", "Information");
                //Get total pages of PF Published products
                pfStocksTotalPages = pfApi.GetPfStocksCount();
                if (pfStocksTotalPages > 0)
                {
                    for (int pfProductPage = 1; pfProductPage <= pfStocksTotalPages; pfProductPage++)
                    {
                        writeLog("Request products From PF By Page Number", "Information");
                        //Export PF product by page number to avoid delay
                        pfResponse = pfApi.RequestProductsByPageNo(pfProductPage);
                        if (!this.ErrorHandler.LogPFApiResponse(pfResponse, pfApi.clientId, this.processId))
                        {

                            XDocument productDoc = XDocument.Parse(pfResponse);
                            //Get the product id and quantity 
                            var productList = from p in productDoc.Descendants("o")
                                              select new
                                              {
                                                  productId = p.Attribute("id").Value,
                                                  quantity = p.Descendants()
                                                             .Where(x => (string)x.Attribute("id") == "233").FirstOrDefault().Value
                                              };
                            foreach (var product in productList)
                            {
                                //Get the tmall product id 
                                string productIdResponse = tmallApi.GetProductId(tmallApi.client, tmallApi.sessionKey, product.productId);
                                if (!string.IsNullOrEmpty(productIdResponse))
                                {
                                    XDocument productIdDoc = XDocument.Parse(productIdResponse);
                                    if (productIdDoc.Element("items_custom_get_response").HasElements)
                                     {
                                        var productInfo = from pId in productIdDoc.Descendants("item")
                                                          select pId.Element("num_iid").Value;
                                        long productId = long.Parse(productInfo.FirstOrDefault());

                                        tmallResponse = tmallApi.UpdateItem(tmallApi.client, tmallApi.sessionKey, productId, long.Parse(product.quantity));
                                        if(string.IsNullOrEmpty(tmallResponse))
                                        {
                                            XDocument itemUpdateDoc = XDocument.Parse(tmallResponse);
                                            if (itemUpdateDoc.Element("item_quantity_update_response").HasElements)
                                            {
                                                writeLog("Item Update success","Information");
                                            }
                                        }
                                        else
                                        {
                                            errorHandler.LogError(new Exception("Tmall UpdateItem API Error"),pfApi.clientId, product.productId);
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            errorHandler.LogError(new Exception("Error while trying to export data from Powerfront API"), pfApi.clientId, "0");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
    #endregion
}