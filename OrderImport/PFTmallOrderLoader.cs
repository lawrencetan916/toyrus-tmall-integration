﻿using System;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace PFTMall
{
    class PFTmallOrderLoader : PFTmallServiceBase
    {
        #region Properties
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        #endregion

        #region Constructor
        public PFTmallOrderLoader(int clientId) : base(clientId)
        {
            processId = "0";
        }
        #endregion

        #region Public Method
        public override void ExecuteTask()
        {
            ExecuteTask(null);
        }

        public override void ExecuteTask(Object o)
        {
            while (runThread)
            {
                try
                {
                    pfApi.LoadClientSettings();
                }
                catch (Exception ex)
                {
                    if (this.DatabaseHandler != null)
                        this.DatabaseHandler.RollBackTransaction();
                    if (this.ErrorHandler != null)
                        this.ErrorHandler.LogError(ex, pfApi.clientId, this.processId);
                }
                this.DatabaseHandler.BeginTransaction();
                try
                {
                    tmallApi = new PFTmallAPI();
                    tmallApi.appKey = pfApi.AppKey;
                    tmallApi.appSecret = pfApi.AppSecret;
                    tmallApi.sessionKey = pfApi.SessionKey;
                    tmallApi.client = pfApi.client;
                    tmallApi.pfAPi = pfApi;

                    /*Determine the value in lastorderbatch
                     * If it's minvalue, set the start time to 2 months before
                     * If the difference between lastorderbatch and current time more than one day, call getOrder API
                     * else, call getincrementorder API
                     */
                    if (pfApi.LastOrderBatch == DateTime.MinValue)
                    {
                        startTime = DateTime.Now.AddMonths(-2);
                        endTime = DateTime.Now;
                        loadOrders(true);
                    }
                    else if(DateTime.Now.Subtract(pfApi.LastOrderBatch)>TimeSpan.FromDays(1))
                    {
                        endTime = DateTime.Now;
                        startTime = pfApi.LastOrderBatch;
                        loadOrders(true);
                    }
                    else
                    {
                        endTime = DateTime.Now;
                        startTime = pfApi.LastOrderBatch;
                        loadOrders(false);
                    }
                    this.DatabaseHandler.CommitTransaction();
                }
                catch (Exception ex)
                {
                    this.DatabaseHandler.RollBackTransaction();
                    this.ErrorHandler.LogError(ex, pfApi.clientId, this.processId);
                }
                Thread.Sleep(TimeSpan.FromSeconds(60));
            }
        }


        private void loadOrders(bool isFirstTime)
        {
            writeLog("Get order from Tmall API", serviceName+"-Information");
            XDocument orderResponseDoc = null;
            //call different API based on if it's firsttime
            try
            {
                if (isFirstTime)
                {
                    orderResponseDoc = tmallApi.GetOrders(tmallApi.client, tmallApi.sessionKey, startTime, endTime);
                }
                else
                {
                    orderResponseDoc = tmallApi.GetIncrementOrders(tmallApi.client, tmallApi.sessionKey, startTime, endTime);
                }
            }
            catch (Exception ex)
            {
                writeLog("Exception raised when trying to load order response from tMall " + ex.Message, serviceName+"-Error");
                return;
            }
            if (pfTmallApi.ExceptionList.Count > 0)
            {
                writeLog("Exception raised when trying to get order from tMall", serviceName + "-Error");
                foreach (Exception ex in pfTmallApi.ExceptionList)
                {
                    this.errorHandler.LogError(ex, pfApi.clientId, "0");
                }
                return;
            }
            //Insert order Id into database
            //Update lastorderbatch
            writeLog("Check if the response contains any order", serviceName + "-Information");
            if (orderResponseDoc != null && orderResponseDoc.Descendants("trade").Count() > 0)
            {
                writeLog("Start looping through all order from the response", serviceName+"-Information");
                foreach (XElement trade in orderResponseDoc.Descendants("trade"))
                {
                    string a = trade.Descendants("tid").FirstOrDefault().Value;
                    databaseHandler.InsertOrder(trade.Descendants("tid").FirstOrDefault().Value);
                }
                writeLog("End looping through all order from the response", serviceName + "-Information");
            }
            databaseHandler.UpdateLastOrderBatch(pfApi.clientId, endTime);
        }
        #endregion
    }
}
