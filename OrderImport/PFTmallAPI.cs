﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using Top.Api;
using Top.Api.Domain;
using Top.Api.Request;
using Top.Api.Response;
using Top.Api.Util;

namespace PFTMall
{
    public class PFTmallAPI
    {
        #region Properties
        PFErrorHandler pfErrorHandler;
        public string appKey { get; set; }
        public string appSecret { get; set; }
        public string sessionKey { get; set; }
        public string refreshToken { get; set; }
        public ITopClient client { get; set; }
        public bool permitCheck = false;
        public PFAPICall pfAPi { get; set; }
        public List<Exception> ExceptionList { get; set; }
        #endregion

        #region Constructor
        public PFTmallAPI()
        {
            pfErrorHandler = new PFErrorHandler();
            ExceptionList = new List<Exception>();
        }
        #endregion

        #region Public Method
        public string PermitRequest(ITopClient client, string sessionKey)
        {
            Console.WriteLine("----permitRequest----");
            string response = "";
            try
            {
                TmcUserPermitRequest permitReq = new TmcUserPermitRequest();
                permitReq.Topics = "taobao_trade_TradeCreate,taobao_trade_TradeSuccess,taobao_trade_TradeBuyerPay,taobao_trade_TradeClose,taobao_trade_TradeAlipayCreate";
                TmcUserPermitResponse permitRsp = client.Execute(permitReq, sessionKey);
                if (permitRsp.IsSuccess)
                {
                    response = permitRsp.Body;
                    permitCheck = true;
                }

            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
            }

            return response;
        }

        public ArrayList ConsumeResponse(ITopClient client, string sessionKey)
        {
            bool check = false;
            Console.WriteLine("----consumeResponse----");
            XmlDocument permitRsp = new XmlDocument();
            ArrayList list = new ArrayList();
            permitRsp.LoadXml(PermitRequest(client, sessionKey));
            string permitStatus = permitRsp.SelectSingleNode(@"/tmc_user_permit_response/is_success").InnerText;
            if (permitStatus.Equals("true"))
            {
                do
                {
                    long msgQuantity = 100L;
                    TmcMessagesConsumeResponse rsp = null;
                    do
                    {
                        TmcMessagesConsumeRequest req = new TmcMessagesConsumeRequest();
                        req.GroupName = "default";
                        req.Quantity = msgQuantity;
                        rsp = client.Execute(req);
                        if (!rsp.IsError && rsp.Messages != null && rsp.Messages.Count > 0)
                        {
                            foreach (TmcMessage msg in rsp.Messages)
                            {
                                if (msg.Topic.Equals("taobao_trade_TradeBuyerPay"))
                                {
                                    list.Add(msg.Content);
                                    check = true;
                                }
                                TmcMessagesConfirmRequest confirmReq = new TmcMessagesConfirmRequest();
                                confirmReq.GroupName = "default";
                                confirmReq.SMessageIds = msg.Id.ToString();
                                TmcMessagesConfirmResponse confirmRsp = client.Execute(confirmReq);
                            }
                            check = false;
                        }
                        else
                        {
                            check = false;
                        }
                    } while (rsp != null && !rsp.IsError && rsp.Messages != null && rsp.Messages.Count == msgQuantity && check == true);
                    Thread.Sleep(new TimeSpan(0, 0, 1));
                } while (check);

            }
            return list;
        }

        //http://open.taobao.com/doc2/apiDetail.htm?apiId=54
        //Retrieve Order Details with order id
        public string GetOrderFullDetails(ITopClient client, string sessionKey, string orderId)
        {
            ExceptionList.Clear();
            string response = "";
            int getResponseTry = 0;
            bool getResponse = true;
            while (getResponse)
            {
                try
                {
                    TradeFullinfoGetRequest orderDetailReq = new TradeFullinfoGetRequest();
                    orderDetailReq.Fields = "tid,orders,buyer_email,receiver_name,receiver_address," +
                                            "receiver_town,receiver_state,receiver_country,receiver_zip,receiver_city,receiver_district," +
                                            "receiver_phone,receiver_mobile,created,pay_time,seller_nick,shipping_type,post_fee";
                    orderDetailReq.Tid = long.Parse(orderId);
                    TradeFullinfoGetResponse orderDetailRsp = client.Execute(orderDetailReq, sessionKey);
                    if (!orderDetailRsp.IsError)
                    {
                        getResponse = false;
                        response = orderDetailRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.Source = "GetOrderDetails->" + ex.Source;
                    ExceptionList.Add(ex);
                }
            }

            return response;
        }

        //Retrieve Authorized Parent ID
        public string GetAuthorizedCategory(ITopClient client, string sessionKey)
        {
            int getResponseTry = 0;
            bool getResponse = true;
            string response = "";
            while (getResponse)
            {
                try
                {
                    ItemcatsAuthorizeGetRequest itemAutCatReq = new ItemcatsAuthorizeGetRequest();
                    itemAutCatReq.Fields = "brand.vid, brand.name, item_cat.cid, item_cat.name, item_cat.status,item_cat.sort_order,item_cat.parent_cid,item_cat.is_parent";
                    ItemcatsAuthorizeGetResponse itemAutCatRsp = client.Execute(itemAutCatReq, sessionKey);
                    if (!itemAutCatRsp.IsError)
                    {
                        getResponse = false;
                        response = itemAutCatRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Retrieve categoty id by provide parent id
        public void GetChildId(ITopClient client, long catId)
        {
            try
            {
                ItemcatsGetRequest itemCatReq = new ItemcatsGetRequest();
                itemCatReq.Fields = "cid,parent_cid,name,is_parent,features";
                itemCatReq.ParentCid = catId;
                ItemcatsGetResponse itemCatRsp = client.Execute(itemCatReq);
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                throw ex;
            }
        }

        public void GetItemProperty(ITopClient client, long catId)
        {
            ItempropsGetRequest itemPropsReq = new ItempropsGetRequest();
            itemPropsReq.Fields = "pid,name,must,multi,prop_values";
            itemPropsReq.Type = 2;
            itemPropsReq.Cid = catId;
            ItempropsGetResponse itemPropsRsp = client.Execute(itemPropsReq);
        }

        public void SearchProduct(ITopClient client, long cid)
        {
            ProductsSearchRequest productSearchReq = new ProductsSearchRequest();
            productSearchReq.Fields = "product_id,name,cid,props";
            productSearchReq.Cid = cid;
            ProductsSearchResponse productSearchRsp = client.Execute(productSearchReq);
        }

        public void AddProduct(ITopClient client, string sessionKey)
        {
            ProductAddRequest productAddReq = new ProductAddRequest();
            productAddReq.Cid = 50067549L;
            productAddReq.Name = "沙箱测试";
            productAddReq.Desc = "沙箱测试";
            productAddReq.OuterId = "3213123";
            productAddReq.Image = new FileItem("C:\\Users\\powerfront\\Pictures\\iphone.jpg");
            ProductAddResponse productAddRsp = client.Execute(productAddReq, sessionKey);
        }

        //Add product from PF to Tmall
        //Set pf product id as tmall product outer id
        public string AddItem(ITopClient client, string sessionKey, string productInfo)
        {
            string response = "";
            XDocument productDoc = new XDocument();
            XmlDocument pDoc = new XmlDocument();
            bool getResponse = true;
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    productDoc = XDocument.Parse(productInfo);
                    pDoc.LoadXml(productInfo);
                    string productCode = productDoc.Descendants()
                    .Where(x => (string)x.Attribute("id") == "143").FirstOrDefault().Value;
                    string ean = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "3143").FirstOrDefault().Value;
                    string productTitle = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "403").FirstOrDefault().Value;
                    string productDesc = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "144").FirstOrDefault().Value;
                    string productPrice = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "220").FirstOrDefault().Value;
                    string stockQuantity = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "233").FirstOrDefault().Value;
                    string picture = productDoc.Descendants()
                       .Where(x => (string)x.Attribute("id") == "446").FirstOrDefault().Value;
                    var productId = from i in productDoc.Descendants("o")
                                    select new
                                    {
                                        Id = (string)i.Attribute("id")
                                    };
                    string id = productId.First().Id;
                    string outerId = id;
                    string picPath = "http://dev.powerfront.com/truchina/www/4003/files/" + picture;
                    ItemAddRequest itemAddReq = new ItemAddRequest();
                    itemAddReq.Cid = 50067658;
                    itemAddReq.Title = "沙箱测试" + productTitle;
                    itemAddReq.Desc = productDesc;
                    itemAddReq.Image = new FileItem("C:\\Users\\powerfront\\Pictures\\iphone.jpg");
                    itemAddReq.Num = long.Parse(stockQuantity);
                    itemAddReq.StuffStatus = "new";
                    itemAddReq.Price = productPrice;
                    itemAddReq.Type = "fixed";
                    itemAddReq.LocationState = "浙江";
                    itemAddReq.LocationCity = "杭州";
                    itemAddReq.ApproveStatus = "onsale";
                    itemAddReq.OuterId = outerId;
                    itemAddReq.HasInvoice = true;
                    itemAddReq.AuctionPoint = 5L;
                    ItemAddResponse itemAddRsp = client.Execute(itemAddReq, sessionKey);
                    if (!itemAddRsp.IsError)
                    {
                        getResponse = false;
                        response = itemAddRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                            pfErrorHandler.LogError(new Exception("Response Timeout From Tmall API AddItem"), pfAPi.clientId, id);
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Get products detail by page number
        public string GetOnsaleItemByPageNo(ITopClient client, string sessionKey, int pageNo)
        {
            string response = "";
            bool getResponse = true;
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    ItemsOnsaleGetRequest itemsOnsaleReq = new ItemsOnsaleGetRequest();
                    itemsOnsaleReq.Fields = "outer_id,num";
                    itemsOnsaleReq.PageNo = pageNo;
                    itemsOnsaleReq.PageSize = 25;
                    ItemsOnsaleGetResponse itemsOnsaleRsp = client.Execute(itemsOnsaleReq, sessionKey);
                    if (!itemsOnsaleRsp.IsError)
                    {
                        getResponse = false;
                        response = itemsOnsaleRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                            pfErrorHandler.LogError(new Exception("Response Timeout From Tmall API GetOnsaleItem"), pfAPi.clientId, "0");
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Get total products
        public string GetOnsaleItem(ITopClient client, string sessionKey)
        {
            string response = "";
            bool getResponse = true;
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    ItemsOnsaleGetRequest itemsOnsaleReq = new ItemsOnsaleGetRequest();
                    itemsOnsaleReq.Fields = "outer_id,num";
                    ItemsOnsaleGetResponse itemsOnsaleRsp = client.Execute(itemsOnsaleReq, sessionKey);
                    if (!itemsOnsaleRsp.IsError)
                    {
                        getResponse = false;
                        response = itemsOnsaleRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                            pfErrorHandler.LogError(new Exception("Response Timeout From Tmall API GetOnsaleItem"), pfAPi.clientId, "0");
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Get product detail by provide product id
        public string GetItemInfo(ITopClient client, string sessionKey, long itemId, string pfProductId)
        {
            bool getResponse = true;
            string response = "";
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    ItemGetRequest req = new ItemGetRequest();
                    req.Fields = "barcode,outer_id,cid,seller_cids,props,input_str,num,list_time,delist_time,stuff_status,approve_status,product_id,num_iid,title,nick,type,desc,skus,props_name,price";
                    req.NumIid = itemId;
                    ItemGetResponse rsp = client.Execute(req, sessionKey);
                    if (!rsp.IsError)
                    {
                        getResponse = false;
                        response = rsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                            pfErrorHandler.LogError(new Exception("Response Timeout at Tmall GetItemInfo API"), pfAPi.clientId, pfProductId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Divide total products by 25 and return total page number
        public int GetOnsaleTotalPages(ITopClient client, string sessionKey)
        {
            string onSaleResponse = GetOnsaleItem(client, sessionKey);
            XDocument onSaleDoc = XDocument.Parse(onSaleResponse);
            var onSaleItems = from items in onSaleDoc.Descendants("total_results")
                              select items.Value;
            int totalPages = int.Parse(onSaleItems.FirstOrDefault());
            return ((int)Math.Ceiling((double)totalPages / (double)25));
        }

        //Update tmall product quantity
        public string UpdateItem(ITopClient client, string sessionKey, long productId, long itemQuantity)
        {
            string response = "";
            bool getResponse = true;
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    ItemQuantityUpdateRequest itemQuantityUpdateReq = new ItemQuantityUpdateRequest();
                    itemQuantityUpdateReq.NumIid = productId;
                    itemQuantityUpdateReq.Quantity = itemQuantity;
                    ItemQuantityUpdateResponse itemQuantityUpdateRsp = client.Execute(itemQuantityUpdateReq, sessionKey);
                    if (!itemQuantityUpdateRsp.IsError)
                    {

                        response = itemQuantityUpdateRsp.Body;
                        getResponse = false;
                    }
                    else
                    {

                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Retrieve tmall product id by provide product outerid(pf product id)
        public string GetProductId(ITopClient client, string sessionKey, string outerId)
        {
            bool getResponse = true;
            string response = "";
            int getResponseTry = 0;
            while (getResponse)
            {
                try
                {
                    ItemsCustomGetRequest itemsCustomReq = new ItemsCustomGetRequest();
                    itemsCustomReq.OuterId = outerId;
                    itemsCustomReq.Fields = "num_iid";
                    ItemsCustomGetResponse itemsCustomRsp = client.Execute(itemsCustomReq, sessionKey);
                    if (!itemsCustomRsp.IsError && itemsCustomRsp.Items.Count > 0)
                    {
                        getResponse = false;
                        response = itemsCustomRsp.Body;
                    }
                    else
                    {
                        getResponseTry += 1;
                        if (getResponseTry == 3)
                        {
                            getResponse = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                    throw ex;
                }
            }
            return response;
        }

        //Update session key with refresh token and modify the config file
        public void UpdateToken(string rToken)
        {
            WebUtils webUtils = new WebUtils();
            IDictionary<string, string> pout = new Dictionary<string, string>();
            pout.Add("grant_type", "refresh_token");
            pout.Add("refresh_token", rToken);
            pout.Add("client_id", "test");
            pout.Add("client_secret", "test");
            string output = webUtils.DoPost("https://oauth.tbsandbox.com/token", pout);
            JObject jObjectOutput = JObject.Parse(output);
            string accessToken = jObjectOutput["access_token"].ToString();
            string refreshToken = jObjectOutput["refresh_token"].ToString();
            try
            {
                string configPath = Path.Combine(System.Environment.CurrentDirectory, "PFTMall.exe");
                Configuration config = ConfigurationManager.OpenExeConfiguration(configPath);
                config.AppSettings.Settings["SessionKey"].Value = accessToken;
                config.AppSettings.Settings["RefreshToken"].Value = refreshToken;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                UpdateConfigKey("SessionKey", accessToken);
                UpdateConfigKey("RefreshToken", refreshToken);
            }
            catch (ConfigurationErrorsException ex)
            {
                pfErrorHandler.LogError(ex, pfAPi.clientId, "0");
                throw ex;
            }
        }

        public void UpdateConfigKey(string strKey, string newValue)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.BaseDirectory + "..\\..\\App.config");
            XmlNode appSettingsNode = xmlDoc.SelectSingleNode("configuration/appSettings");

            foreach (XmlNode childNode in appSettingsNode)
            {
                if (childNode.Attributes["key"].Value == strKey)
                {
                    childNode.Attributes["value"].Value = newValue;
                }
            }
            xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "..\\..\\App.config");
            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
        }

        //First Get Orders API call, retrieve the orders up to 3 months
        //Convert the timezone from AUS to China
        public XDocument GetOrders(ITopClient client, string sessionKey, DateTime startTime, DateTime endTime)
        {
            ExceptionList.Clear();
            startTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(startTime, TimeZoneInfo.Local.Id, "China Standard Time");
            endTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(endTime, TimeZoneInfo.Local.Id, "China Standard Time");

            bool hasNext = true;
            long pageNo = 0;
            XDocument doc = null;
            int getResponseTry = 0;
            bool getResponse = true;
            while (hasNext)
            {
                getResponse = true;
                while (getResponse)
                {
                    try
                    {
                        pageNo++;
                        TradesSoldGetRequest req = new TradesSoldGetRequest();
                        req.PageNo = pageNo;
                        req.Fields = "tid";
                        req.UseHasNext = true;
                        req.Status = "WAIT_SELLER_SEND_GOODS";
                        req.StartCreated = startTime;
                        req.EndCreated = endTime;
                        TradesSoldGetResponse rsp = client.Execute(req, sessionKey);
                        if (!rsp.IsError)
                        {
                            getResponse = false;
                            var ori = XDocument.Parse(rsp.Body);
                            hasNext = bool.Parse(ori.Descendants("has_next").FirstOrDefault().Value);
                            if (pageNo == 1)
                            {
                                doc = XDocument.Parse(rsp.Body);
                            }
                            else
                            {
                                doc.Descendants("trade").LastOrDefault().AddAfterSelf(ori.Descendants("trade"));
                            }
                        }
                        else
                        {
                            getResponseTry += 1;
                            if (getResponseTry == 3)
                            {
                                getResponse = false;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Source = "GetOrder->" + ex.Source;
                        ExceptionList.Add(ex);
                    }
                }
            }
            return doc;
        }

        //Increment order call to retrieve orders up to 1 day
        public XDocument GetIncrementOrders(ITopClient client, string sessionKey, DateTime startTime, DateTime endTime)
        {
            ExceptionList.Clear();
            startTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(startTime, TimeZoneInfo.Local.Id, "China Standard Time");
            startTime.AddMinutes(-5);
            endTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(endTime, TimeZoneInfo.Local.Id, "China Standard Time");

            bool hasNext = true;
            long pageNo = 0;
            XDocument doc = null;
            int getResponseTry = 0;
            bool getResponse = true;
            while (hasNext)
            {
                getResponse = true;
                while (getResponse)
                {
                    try
                    {
                        pageNo++;
                        TradesSoldIncrementGetRequest req = new TradesSoldIncrementGetRequest();
                        req.PageNo = pageNo;
                        req.Fields = "tid";
                        req.UseHasNext = true;
                        req.Status = "WAIT_SELLER_SEND_GOODS";
                        req.StartModified = startTime;
                        req.EndModified = endTime;
                        TradesSoldIncrementGetResponse rsp = client.Execute(req, sessionKey);
                        if (!rsp.IsError)
                        {
                            getResponse = false;
                            var ori = XDocument.Parse(rsp.Body);
                            hasNext = bool.Parse(ori.Descendants("has_next").FirstOrDefault().Value);
                            if (pageNo == 1)
                            {
                                doc = XDocument.Parse(rsp.Body);
                            }
                            else
                            {
                                doc.Descendants("trade").LastOrDefault().AddAfterSelf(ori.Descendants("trade"));
                            }
                        }
                        else
                        {
                            getResponseTry += 1;
                            if (getResponseTry == 3)
                            {
                                getResponse = false;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Source = "GetIncrementOrder->" + ex.Source;
                        ExceptionList.Add(ex);
                    }
                }
            }
            return doc;
        }
    }
    #endregion
}