﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PFTMall
{
    public class PFDatabaseHandler
    {
        private SqlConnection sqlConnection;
        private SqlTransaction sqlTransaction;
        private const int cmdTimeout = 300;

        public string connectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["PFTmallConnectionString"].ConnectionString;
            }
        }
        public PFDatabaseHandler()
        {
            sqlConnection = new SqlConnection(connectionString);
        }

        public void BeginTransaction()
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
            if (sqlTransaction == null)
            {
                sqlTransaction = sqlConnection.BeginTransaction();
            }
        }

        public void CommitTransaction()
        {
            if (sqlTransaction != null && sqlTransaction.Connection != null)
            {
                sqlTransaction.Commit();
                sqlTransaction = null;
            }
            if (sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
            }
        }

        public void RollBackTransaction()
        {
            if (sqlTransaction != null && sqlTransaction.Connection != null)
            {
                sqlTransaction.Rollback();
                sqlTransaction = null;
            }

            if (sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
        }

        public int InsertOrder(string orderID)
        {
            int result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = cmdTimeout;
                cmd.Connection = sqlConnection;
                cmd.Transaction = sqlTransaction;
                cmd.CommandType = CommandType.Text;
                StringBuilder sqlBuild = new StringBuilder();

                sqlBuild.AppendLine("if not exists (select orderid from orders (NOLOCK) where orderid = @orderid)");
                sqlBuild.AppendLine("begin");
                sqlBuild.AppendLine("insert into orders(orderid)");
                sqlBuild.AppendLine("values(@orderid)");
                sqlBuild.AppendLine("end");

                cmd.CommandText = sqlBuild.ToString();

                cmd.Parameters.Clear();

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@orderid";
                param.Value = orderID;
                cmd.Parameters.Add(param);

                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public DataTable GetOrder(int clientID)
        {
            DataTable result = null;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = cmdTimeout;
                cmd.Connection = sqlConnection;
                cmd.Transaction = sqlTransaction;
                cmd.CommandType = CommandType.Text;
                StringBuilder sqlBuild = new StringBuilder();

                sqlBuild.AppendLine("select top 1000 * from orders");

                cmd.CommandText = sqlBuild.ToString();

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = cmd;
                DataSet resultSet = new DataSet();
                adapter.Fill(resultSet);

                if (resultSet.Tables.Count > 0)
                    result = resultSet.Tables[0];


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// this method is used to insert order line
        /// </summary>
        /// <param name="orderlineid"></param>
        /// <param name="orderID"></param>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public int InsertOrderLine(string orderlineid, string orderID, int clientID)
        {
            int result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = cmdTimeout;
                cmd.Connection = sqlConnection;
                cmd.Transaction = sqlTransaction;
                cmd.CommandType = CommandType.Text;
                StringBuilder sqlBuild = new StringBuilder();

                sqlBuild.AppendLine("if not exists (select orderlineid from orderlines (NOLOCK) where orderlineid = @orderlineid)");
                sqlBuild.AppendLine("begin");
                sqlBuild.AppendLine("insert into orderlines(orderlineid, orderid, clientid)");
                sqlBuild.AppendLine("values(@orderlineid,@orderid,@clientid)");
                sqlBuild.AppendLine("end");
                cmd.CommandText = sqlBuild.ToString();

                cmd.Parameters.Clear();
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@orderlineid";
                param.Value = orderlineid;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@orderid";
                param.Value = orderID;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@clientid";
                param.Value = clientID;
                cmd.Parameters.Add(param);
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public int InsertErrorLog(int clientId, short errorType, int errorCode, string errorDescription,
            string productId, string errorDateTime)
        {
            int result = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandTimeout = cmdTimeout;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = sqlTransaction;
                sqlCommand.CommandType = CommandType.Text;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("insert into errors(clientid, errortype, errorcode, errordescription, ");
                stringBuilder.AppendLine("productid, datetime)");
                stringBuilder.AppendLine("values(@clientid,@errortype,@errorcode,@errordescription,@productid, @datetime)");
                sqlCommand.CommandText = stringBuilder.ToString();
                sqlCommand.Parameters.Clear();

                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@clientid";
                sqlParameter.Value = clientId;
                sqlCommand.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@errortype";
                sqlParameter.Value = errorType;
                sqlCommand.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@errorcode";
                sqlParameter.Value = errorCode;
                sqlCommand.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@errordescription";
                sqlParameter.Value = errorDescription;
                sqlCommand.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@productid";
                if (productId == null)
                {
                    sqlParameter.Value = DBNull.Value;
                }
                else
                {
                    sqlParameter.Value = productId;
                }
                sqlCommand.Parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = "@datetime";
                sqlParameter.Value = Convert.ToDateTime(errorDateTime);
                sqlCommand.Parameters.Add(sqlParameter);

                result = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public int UpdateLastOrderBatch(int clientID, DateTime lastOrderBatch)
        {
            int result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = cmdTimeout;
                cmd.Connection = sqlConnection;
                cmd.Transaction = sqlTransaction;
                cmd.CommandType = CommandType.Text;
                StringBuilder sqlBuild = new StringBuilder();

                sqlBuild.AppendLine("update clients set lastorderbatch = @lastorderbatch");
                sqlBuild.AppendLine("where clientid = @clientid");

                cmd.CommandText = sqlBuild.ToString();

                cmd.Parameters.Clear();

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@clientid";
                param.Value = clientID;
                cmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@lastorderbatch";
                param.Value = lastOrderBatch;
                cmd.Parameters.Add(param);

                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public DataTable GetClientData(int clientID)
        {
            DataTable result = null;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = cmdTimeout;
                cmd.Connection = sqlConnection;
                cmd.Transaction = sqlTransaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from clients (NOLOCK) where clientid = " + clientID.ToString();

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = cmd;
                DataSet resultSet = new DataSet();
                adapter.Fill(resultSet);

                if (resultSet.Tables.Count > 0)
                    result = resultSet.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }


}
