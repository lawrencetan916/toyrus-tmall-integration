﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Top.Api;
using System.Configuration;
using System.Data;

namespace PFTMall
{
    public class PFAPICall
    {
        #region Properties
        PFErrorHandler pfErrorHandler;
        public int clientId { get; set; }
        public ITopClient client { get; set; }
        public string serverUrl { get; set; }
        public PFDatabaseHandler pfDatabaseHandler { get; set; }
        private string pfApiUsername;
        public string PFApiUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["PFApiUsername"];
            }
        }

        private string pfApiPassword;
        public string PFApiPassword
        {
            get
            {
                //return pfApiPassword;
                return ConfigurationManager.AppSettings["pfApiPassword"];
            }
        }

        private string pfApiServer;
        public string PFApiServer
        {
            get
            {
                return pfApiServer;
            }
        }

        private DataTable singleProductData;
        public DataTable SingleProductData
        {
            get
            {
                return singleProductData;
            }
        }
        private string pfApiObjectImport;
        public string PFApiObjectImport
        {
            get
            {
                return pfApiObjectImport;
            }
        }

        private string pfApiTransImport;
        public string PFApiTransImport
        {
            get
            {
                return pfApiTransImport;
            }
        }
        private string pfApiObjectExport;
        public string PFApiObjectExport
        {
            get
            {
                return pfApiObjectExport;
            }
        }
        public string ObjectExportService
        {
            get
            {
                return ConfigurationManager.AppSettings["ObjectExportService"];
            }
        }

        public string ObjectImportService
        {
            get
            {
                return ConfigurationManager.AppSettings["ObjectImportService"];
            }
        }

        public string TransImportService
        {
            get
            {
                return ConfigurationManager.AppSettings["TransImportService"];
            }
        }
        private string appKey;
        public string AppKey
        {
            get
            {
                return appKey;
            }
        }
        private string appSecret;
        public string AppSecret
        {
            get
            {
                return appSecret;
            }
        }
        private string sessionKey;
        public string SessionKey
        {
            get
            {
                return sessionKey;
            }
        }
        private string refreshToken;
        public string RefreshToken
        {
            get
            {
                return refreshToken;
            }
        }
        private DateTime lastOrderBatch;
        public DateTime LastOrderBatch
        {
            get
            {
                return lastOrderBatch;
            }
        }
        private DataTable clientData;
        public DataTable ClientData
        {
            get
            {
                return clientData;
            }
        }
        #endregion
        #region Constructor
        public PFAPICall()
        {
            pfErrorHandler = new PFErrorHandler();
        }
        #endregion
        #region Public Method

        /*Initial setup required parameters
         */
        public void LoadClientSettings()
        {
            try
            {
                clientData = pfDatabaseHandler.GetClientData(clientId);
                DataRow row = clientData.Rows[0];
                pfApiUsername = ConfigurationManager.AppSettings["pfApiUsername"];
                pfApiPassword = ConfigurationManager.AppSettings["pfApiPassword"];
                pfApiServer = ConfigurationManager.AppSettings["PFServerUrl"];
                pfApiObjectExport = pfApiServer + ObjectExportService;
                pfApiObjectImport = pfApiServer + ObjectImportService;
                pfApiTransImport = pfApiServer + TransImportService;
                serverUrl = ConfigurationManager.AppSettings["TmallUrl"];
                appKey = ConfigurationManager.AppSettings["AppKey"];
                appSecret = ConfigurationManager.AppSettings["AppSecret"];
                sessionKey = ConfigurationManager.AppSettings["SessionKey"];
                refreshToken = ConfigurationManager.AppSettings["RefreshToken"];
                if (row["lastorderbatch"] == DBNull.Value)
                {
                    lastOrderBatch = DateTime.MinValue;
                }
                else
                {
                    DateTime.TryParse(row["lastorderbatch"].ToString(), out lastOrderBatch);
                }
                client = new DefaultTopClient(serverUrl, appKey, appSecret);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Validate the path of xml is not null
        public string CheckXMLNode(XmlDocument doc, string path)
        {
            string defaultValue = String.Empty;
            if (doc.SelectSingleNode(path) != null)
            {
                defaultValue = doc.SelectSingleNode(path).InnerText;
                return defaultValue;
            }
            return defaultValue;
        }

        //Retrieve partner if exists, else create new partner
        //Call PF Export API to retrieve partner with partner email address
        public string GetOrCreatePartner(XmlDocument doc)
        {
            string partnerId = String.Empty;
            bool isValid = false;
            string responseString = String.Empty;
            string buyerEmail = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/buyer_email");
            string buyerAddress = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/receiver_address");
            string addressState = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/receiver_state");
            string addressCity = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/receiver_city");
            string addressDistrict = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/receiver_district");
            string addressPostcode = CheckXMLNode(doc, @"/trade_fullinfo_get_response/trade/receiver_zip");
            isValid = (!string.IsNullOrEmpty(buyerEmail)) ? true : false;
            if (isValid)
            {
                try
                {
                    string webServiceURL = pfApiObjectExport;
                    HttpWebRequest request = WebRequest.Create(webServiceURL) as HttpWebRequest;
                    string xmlRequest = "<export>" +
                        "  <sec cid=\"" + clientId + "\">" +
                        "   <login usr=\"" + pfApiUsername + "\" pass=\"" + pfApiPassword + "\" />" +
                        "  </sec>" +
                        "  <params>" +
                        "    <tid>18</tid>" +
                        "    <stat>published</stat>" +
                        "  </params>" +
                        "  <prop>" +
                        "     <p id=\"333\" cond=\"v='" + buyerEmail + "'\" />" +
                        "     <p id=\"30\" />" +
                        "</prop>" +
                        "</export>";
                    pfErrorHandler.LogText(xmlRequest, clientId);
                    Byte[] bytes = Encoding.UTF8.GetBytes(xmlRequest);
                    Console.WriteLine(xmlRequest);
                    request.Method = "POST";
                    request.ContentLength = bytes.Length;
                    request.ContentType = "text/xml";

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();

                    int getResponseTry = 0;
                    bool getResponse = true;

                    while (getResponse)
                    {
                        try
                        {
                            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                            {
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    getResponse = false;
                                    StreamReader reader = new StreamReader(response.GetResponseStream());
                                    responseString = reader.ReadToEnd();
                                    XmlDocument xmlItemDoc = new XmlDocument();
                                    xmlItemDoc.LoadXml(responseString);

                                    XmlNode varsNode = xmlItemDoc.SelectSingleNode(@"/export/resp");
                                    int recCount = 0;
                                    if (varsNode != null)
                                    {
                                        XmlAttribute rCountAtt = varsNode.Attributes["recs"];
                                        if (rCountAtt != null)
                                        {
                                            recCount = Convert.ToInt32(rCountAtt.Value);
                                        }
                                    }

                                    if (recCount > 0)
                                    {
                                        XmlNodeList partnerNode = xmlItemDoc.SelectNodes(@"/export/obj/o");
                                        foreach (XmlNode xndNode in partnerNode)
                                        {
                                            XmlNode enableAccountPropNode = xndNode.SelectSingleNode(@"prop/p[@id='30']");
                                            if (enableAccountPropNode != null)
                                            {
                                                XmlNode enableAccountNode = enableAccountPropNode.SelectSingleNode(@"val/v");
                                                if (enableAccountNode == null)
                                                {
                                                    Console.WriteLine("Enable Account Node = null");
                                                }
                                                else
                                                {
                                                    if (enableAccountNode.InnerText == "True")
                                                    {
                                                        partnerId = xndNode.Attributes["id"].Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        partnerId = ImportPartner(doc);
                                    }
                                }
                                else
                                {
                                    getResponseTry += 1;
                                    if (getResponseTry == 3)
                                    {
                                        getResponse = false;
                                    }
                                }
                            }
                        }
                        catch (WebException webEx)
                        {
                            if (webEx.Status != WebExceptionStatus.Success)
                            {
                                pfErrorHandler.LogError(webEx, clientId, "0");
                                getResponseTry += 1;
                                if (getResponseTry == 3)
                                {
                                    getResponse = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            pfErrorHandler.LogError(ex, clientId, "0");
                            throw ex;
                        }
                    }
                }
                catch (Exception ex)
                {
                    pfErrorHandler.LogError(ex, clientId, "0");
                    throw ex;
                }
            }
            return partnerId;
        }

        public void LoadClientOrderData()
        {
            try
            {
                singleProductData = pfDatabaseHandler.GetOrder(clientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Import order into PF 
        public string ImportOrder(XmlDocument orderDoc)
        {
            string responseString = String.Empty;
            int clientID = clientId;
            try
            {
                string webServiceURL = pfApiTransImport;
                HttpWebRequest request = WebRequest.Create(webServiceURL) as HttpWebRequest;
                XmlTextWriter writer = new XmlTextWriter(Console.Out);
                writer.Formatting = Formatting.Indented;
                string strXmlText = String.Empty;

                using (StringWriter sw = new StringWriter())
                {
                    using (XmlTextWriter tx = new XmlTextWriter(sw))
                    {
                        orderDoc.WriteTo(tx);
                        strXmlText = sw.ToString();
                        pfErrorHandler.LogText(strXmlText, clientId);
                    }
                }

                string orderId = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/orders/order/oid");
                string receiverName = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_name");
                string deliveryStreetAddress = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_address");
                string deliveryPostcode = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_zip");
                string deliveryState = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_state");
                string deliveryDistrict = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_district");
                string deliveryTown = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_town");
                string deliveryCity = CheckXMLNode(orderDoc, (@"/trade_fullinfo_get_response/trade/receiver_city"));
                string deliveryPhone = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_phone");
                string deliveryMobile = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_mobile");
                string country = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_country");
                string deliveryCountry = (string.IsNullOrEmpty(country)) ? "China" : country;
                string phoneNumber = (string.IsNullOrEmpty(deliveryPhone)) ? deliveryMobile : deliveryPhone;
                string partnerEmail = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/buyer_email");
                string shippingMethod = CheckXMLNode(orderDoc, (@"/trade_fullinfo_get_response/trade/shipping_type"));
                string shippingCost = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/post_fee");
                string orderCreatedTime = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/created");
                string orderPaidTime = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/pay_time");
                string partnerId = GetOrCreatePartner(orderDoc);
                XmlNodeList orderList = orderDoc.SelectNodes(@"/trade_fullinfo_get_response/trade/orders/order");
                if (string.IsNullOrEmpty(partnerId))
                {
                    Console.WriteLine("Empty Partner");
                }
                else
                {
                    Console.WriteLine("Retrieved Partner");
                    DateTime entryDate;
                    DateTime.TryParse(orderPaidTime, out entryDate);
                    StringBuilder xmlBuilder = new StringBuilder();

                    xmlBuilder.AppendLine("<?xml version=\"1.0\"?>");
                    xmlBuilder.AppendLine("<transactions>");
                    xmlBuilder.AppendLine(string.Format("<sec cid=\"{0}\" >", clientId));
                    xmlBuilder.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                    xmlBuilder.AppendLine("</sec>");
                    xmlBuilder.AppendLine("<params>");
                    xmlBuilder.AppendLine("<checkpartner>false</checkpartner>");
                    xmlBuilder.AppendLine("<checkpaymentterms>false</checkpaymentterms>");
                    xmlBuilder.AppendLine("<checkshippingmethod>false</checkshippingmethod>");
                    if (clientId == 4003)
                    {
                        string tmallTransactionId = orderDoc.SelectSingleNode(@"/trade_fullinfo_get_response/trade/tid").InnerText;
  
                    }
                    xmlBuilder.AppendLine("</params>");
                    xmlBuilder.AppendLine("<transaction>");
                    xmlBuilder.AppendLine("<transactiontype>303</transactiontype>");
                    xmlBuilder.AppendLine("<transactionrkey>" + orderId + "</transactionrkey>");
                    xmlBuilder.AppendLine("<entrydate>" + entryDate.ToLocalTime().ToString("yyyy-MM-dd") + "</entrydate>");
                    xmlBuilder.AppendLine("<status>33</status>");
                    xmlBuilder.AppendLine("<partnername><![CDATA[" + receiverName + "]]></partnername>");
                    xmlBuilder.AppendLine("<partnerid>" + partnerId + "</partnerid>");
                    xmlBuilder.AppendLine("<billingaddress><![CDATA[" + deliveryStreetAddress + "]]></billingaddress>");
                    //xmlBuilder.AppendLine("<billingtown>" + deliveryTown + "</billingtown>");
                    //xmlBuilder.AppendLine("<billingdistrict><![CDATA[" + deliveryDistrict + "]]></billingdistrict>");
                    xmlBuilder.AppendLine("<billingsuburb>" + deliveryCity + "</billingsuburb>");
                    xmlBuilder.AppendLine("<billingstate>" + deliveryState + "</billingstate>");
                    xmlBuilder.AppendLine("<billingpostcode>" + deliveryPostcode + "</billingpostcode>");
                    xmlBuilder.AppendLine("<billingcountry>" + deliveryCountry + "</billingcountry>"); // US throws error
                    xmlBuilder.AppendLine("<billingemail>" + partnerEmail + "</billingemail>");
                    xmlBuilder.AppendLine("<billingphone><![CDATA[" + phoneNumber + "]]></billingphone>");
                    xmlBuilder.AppendLine("<shippingcontactphone><![CDATA[" + phoneNumber + "]]></shippingcontactphone>");
                    xmlBuilder.AppendLine("<billingmobile><![CDATA[" + phoneNumber + "]]></billingmobile>");
                    xmlBuilder.AppendLine("<billingfax></billingfax>"); 
                    xmlBuilder.AppendLine("<shippingaddress><![CDATA[" + deliveryStreetAddress + "]]></shippingaddress>");
                    //xmlBuilder.AppendLine("<shippingtown>" + deliveryTown + "</shippingtown>");
                    //xmlBuilder.AppendLine("<shippingdistrict><![CDATA[" + deliveryDistrict + "]]></shippingdistrict>");
                    xmlBuilder.AppendLine("<shippingsuburb>" + deliveryCity + "</shippingsuburb>");
                    xmlBuilder.AppendLine("<shippingstate>" + deliveryState + "</shippingstate>");
                    xmlBuilder.AppendLine("<shippingpostcode>" + deliveryPostcode + "</shippingpostcode>");
                    xmlBuilder.AppendLine("<shippingcountry>" + deliveryCountry + "</shippingcountry>");
                    xmlBuilder.AppendLine("<shippingcountrytaxexempt>false</shippingcountrytaxexempt>"); // TODO: NOT SURE
                    xmlBuilder.AppendLine("<shippingmethod>" + shippingMethod + "</shippingmethod>");
                    xmlBuilder.AppendLine("<shippingtotal>" + shippingCost + "</shippingtotal>");
                    xmlBuilder.AppendLine("<paymentterms></paymentterms>");
                    xmlBuilder.AppendLine("<termsdiscount>0.00</termsdiscount>");
                    xmlBuilder.AppendLine("<termsdiscountamount>0.00</termsdiscountamount>");
                    xmlBuilder.AppendLine("<termssurcharge>0.00</termssurcharge>"); 
                    xmlBuilder.AppendLine("<termssurchargeamount>0.00</termssurchargeamount>");
                    xmlBuilder.AppendLine("<discount>0.00</discount>");
                    xmlBuilder.AppendLine("<discountamount>0.00</discountamount>"); 
                    xmlBuilder.AppendLine("<taxrate>10.00</taxrate>"); 
                    xmlBuilder.AppendLine("<warehousefrom>335</warehousefrom>");
                    xmlBuilder.AppendLine("<referrer>tmall</referrer>");
                    xmlBuilder.AppendLine("<transactionlines>");
                    foreach (XmlNode order in orderList)
                    {
                        string outerId = order.SelectSingleNode(@"./outer_iid").InnerText;
                        string productID = String.Empty;
                        string skn = String.Empty;
                        if (outerId != String.Empty && outerId.Contains(','))
                        {
                            productID = outerId.Split(',')[0];
                            skn = outerId.Split(',')[1];
                        }
                        string productTitle = order.SelectSingleNode(@"./title").InnerText;

                        string productQuantity = order.SelectSingleNode(@"./num").InnerText;
                        string productPrice = order.SelectSingleNode(@"./price").InnerText;
                        xmlBuilder.AppendLine("<transactionline>");
                        xmlBuilder.AppendLine("<transactionlinerkey>" + orderId + "</transactionlinerkey>");
                        xmlBuilder.AppendLine("<productid>" + productID + "</productid>");
                        xmlBuilder.AppendLine("<productdescription><![CDATA[" + productTitle + "]]></productdescription>");
                        xmlBuilder.AppendLine("<qty>" + productQuantity + "</qty>");
                        xmlBuilder.AppendLine("<unitprice>" + productPrice + "</unitprice>");
                        xmlBuilder.AppendLine("<tax>0.0</tax>"); 
                        xmlBuilder.AppendLine("<taxrate>0.0</taxrate>"); 
                        xmlBuilder.AppendLine("<variations></variations>");
                        xmlBuilder.AppendLine("<configurationopts></configurationopts>"); 
                        xmlBuilder.AppendLine("</transactionline>");
                    }
                    xmlBuilder.AppendLine("</transactionlines>");
                    xmlBuilder.AppendLine("</transaction>");
                    xmlBuilder.AppendLine("</transactions>");

                    string xmlRequest = xmlBuilder.ToString();
                    pfErrorHandler.LogText(xmlRequest, clientId);
                    Byte[] bytes = Encoding.UTF8.GetBytes(xmlRequest);

                    request.Method = "POST";
                    request.ContentLength = bytes.Length;
                    request.ContentType = "text/xml";

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();

                    int getResponseTry = 0;
                    bool getResponse = true;
                    while (getResponse)
                    {
                        try
                        {
                            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                            {
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    getResponse = false;
                                    StreamReader reader = new StreamReader(response.GetResponseStream());
                                    responseString = reader.ReadToEnd();
                                }
                                else
                                {
                                    getResponseTry += 1;
                                    if (getResponseTry == 3)
                                    {
                                        getResponse = false;
                                    }
                                }
                            }
                        }
                        catch (WebException webEx)
                        {
                            if (webEx.Status != WebExceptionStatus.Success)
                            {
                                pfErrorHandler.LogError(webEx, clientId, "0");
                                getResponseTry += 1;
                                if (getResponseTry == 3)
                                {
                                    getResponse = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            pfErrorHandler.LogError(ex, clientId, "0");
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }
            return responseString;
        }

        //Create new partner
        public string ImportPartner(XmlDocument orderDoc)
        {
            string responseString = String.Empty;
            string partnerId = String.Empty;
            string partnerEmail = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/buyer_email");
            string firstName = String.Empty;
            string lastName = String.Empty;
            string receiverName = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_name");
            string deliveryStreetAddress = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_address");
            string deliveryPostcode = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_zip");
            string deliveryState = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_state");
            string deliveryDistrict = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_district");
            string deliveryTown = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_town");
            string deliveryCity = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_city");
            string deliveryPhone = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_phone");
            string deliveryMobile = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_mobile");
            string deliveryCountry = CheckXMLNode(orderDoc, @"/trade_fullinfo_get_response/trade/receiver_country");
            string phoneNumber = (string.IsNullOrEmpty(deliveryPhone)) ? deliveryMobile : deliveryPhone;
            string country = (string.IsNullOrEmpty(deliveryCountry)) ? "China" : deliveryCountry;
            string[] names = receiverName.Select(x => x.ToString()).ToArray();
            if (!string.IsNullOrEmpty(receiverName))
            {
                firstName = receiverName.Select(x => x.ToString()).FirstOrDefault().ToString();
                lastName = receiverName.Substring(1);
            }
            try
            {
                String webServiceURL = pfApiObjectImport;
                HttpWebRequest request = WebRequest.Create(webServiceURL) as HttpWebRequest;
                StringBuilder xmlBuilder = new StringBuilder();
                int clientID = 4003;
                xmlBuilder.AppendLine("<?xml version=\"1.0\"?>");
                xmlBuilder.AppendLine("<import>");
                xmlBuilder.AppendLine(string.Format("<sec cid=\"{0}\" >", clientID));
                xmlBuilder.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                xmlBuilder.AppendLine("</sec>");
                xmlBuilder.AppendLine("<params />");
                xmlBuilder.AppendLine("<obj>");
                xmlBuilder.AppendLine("<o tid=\"18\">");
                xmlBuilder.AppendLine("<prop>");
                xmlBuilder.AppendLine("<p id=\"70\" nm=\"Type\"><val><v>327</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"72\" nm=\"First name\"><val><v>" + lastName + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"73\" nm=\"Surname\"><val><v><![CDATA[" + firstName + "]]></v></val></p>");
                xmlBuilder.AppendLine("<p id=\"74\" nm=\"Billing street address\"><val><v><![CDATA[" + deliveryStreetAddress + "]]></v></val></p>");
                xmlBuilder.AppendLine("<p id=\"11001893\" nm=\"Billing district\"><val><v>" + deliveryDistrict + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"11001894\" nm=\"Billing town\"><val><v>" + deliveryTown + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"75\" nm=\"Billing suburb city\"><val><v>" + deliveryCity + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"76\" nm=\"Billing state province\"><val><v>" + deliveryState + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"77\" nm=\"Billing country\"><val><v>" + country + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"78\" nm=\"Billing post code\"><val><v>" + deliveryPostcode + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"79\" nm=\"Billing phone\"><val><v>" + phoneNumber + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"81\" nm=\"Delivery street address\"><val><v><![CDATA[" + deliveryStreetAddress + "]]></v></val></p>");
                xmlBuilder.AppendLine("<p id=\"11001895\" nm=\"Delivery district\"><val><v>" + deliveryDistrict + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"11001896\" nm=\"Delivery town\"><val><v>" + deliveryTown + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"82\" nm=\"Delivery suburb city\"><val><v>" + deliveryCity + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"83\" nm=\"Delivery state province\"><val><v>" + deliveryState + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"84\" nm=\"Delivery country\"><val><v>" + country + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"85\" nm=\"Delivery post code\"><val><v>" + deliveryPostcode + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"86\" nm=\"Delivery phone\"><val><v>" + phoneNumber + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"89\" nm=\"Email Address\"><val><v>" + partnerEmail + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"333\" nm=\"Login\"><val><v>" + partnerEmail + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"321\" nm=\"Password\"><val><v>" + GetUniqueKey(8) + "</v></val></p>");
                xmlBuilder.AppendLine("<p id=\"3047\" nm=\"Referrer\"><val><v>tmall</v></val></p>");
                xmlBuilder.AppendLine("</prop>");
                xmlBuilder.AppendLine("</o>");
                xmlBuilder.AppendLine("</obj>");
                xmlBuilder.AppendLine("</import>");

                string xmlRequest = xmlBuilder.ToString();
                pfErrorHandler.LogText(xmlRequest, clientId);

                Byte[] bytes = Encoding.UTF8.GetBytes(xmlRequest);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                int getResponseTry = 0;
                bool getResponse = true;
                while (getResponse)
                {
                    try
                    {
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                getResponse = false;
                                StreamReader reader = new StreamReader(response.GetResponseStream());

                                responseString = reader.ReadToEnd();
                                XmlDocument xmlItemDoc = new XmlDocument();
                                xmlItemDoc.LoadXml(responseString);

                                XmlNode varsNode = xmlItemDoc.SelectSingleNode(@"/import/obj/success/o");
                                if (varsNode != null)
                                {
                                    XmlAttribute partnerIdAttr = varsNode.Attributes["id"];
                                    if (partnerIdAttr != null)
                                    {
                                        partnerId = partnerIdAttr.Value;
                                    }
                                }
                            }
                            else
                            {
                                getResponseTry += 1;
                                if (getResponseTry == 3)
                                {
                                    getResponse = false;
                                }
                            }
                        }
                    }
                    catch (WebException webEx)
                    {
                        if (webEx.Status != WebExceptionStatus.Success)
                        {
                            pfErrorHandler.LogError(webEx, clientId, "0");
                            getResponseTry += 1;
                            if (getResponseTry == 3)
                            {
                                getResponse = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        pfErrorHandler.LogError(ex, clientId, "0");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }
            return partnerId;
        }

        //Call PF Export to retrieve product information by product id
        public string RequestProductInfoById(string productId)
        {
            string result = String.Empty;
            string responseString = string.Empty;
            int clientId = 4003;
            try
            {
                StringBuilder pfRequest = new StringBuilder();
                string webServiceUrl = pfApiObjectExport;
                pfRequest.AppendLine("<export>");
                pfRequest.AppendLine(string.Format("<sec cid=\"{0}\">", clientId));
                pfRequest.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                pfRequest.AppendLine("</sec>");
                pfRequest.AppendLine("<params>");
                pfRequest.AppendLine("<tid>15</tid>");
                pfRequest.AppendLine("<stat>published</stat>");
                pfRequest.AppendLine(string.Format("<ids>{0}</ids>", productId));
                pfRequest.AppendLine("</params>");
                pfRequest.AppendLine("<prop>");
                pfRequest.AppendLine("<p id=\"450\" nm=\"product type\" />");
                pfRequest.AppendLine("<p id=\"143\" nm=\"product code\" />");
                pfRequest.AppendLine("<p id=\"3143\" nm=\"ean\" />");
                pfRequest.AppendLine("<p id=\"11001898\" nm=\"tmall id\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"style\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"hero code\" />");
                pfRequest.AppendLine("<p id=\"2730\" nm=\"hero product\" />");
                pfRequest.AppendLine("<p id=\"403\" nm=\"web product name\" />");
                pfRequest.AppendLine("<p id=\"1741\" nm=\"product summary\" />");
                pfRequest.AppendLine("<p id=\"144\" nm=\"product description\" />");
                pfRequest.AppendLine("<p id=\"391\" nm=\"variation\" roids=\"1002099,1002100\" ronms=\"colour,size\" />");
                pfRequest.AppendLine("<p id=\"220\" nm=\"price\" roids=\"249\" ronms=\"retail\" />");
                pfRequest.AppendLine("<p id=\"233\" nm=\"soh\" />");
                pfRequest.AppendLine("<p id=\"447\" nm=\"image1\" />");
                pfRequest.AppendLine("<p id=\"1952\" nm=\"start sale date\" />");
                pfRequest.AppendLine("<p id=\"1953\" nm=\"end sale date\" />");
                pfRequest.AppendLine("<p id=\"161\" nm=\"category\" sw=\"returnid\" />");
                pfRequest.AppendLine("<p id=\"446\" nm=\"image thumbnail\" />");
                pfRequest.AppendLine("<p id=\"1576\" />");
                pfRequest.AppendLine("</prop>");
                pfRequest.AppendLine("</export>");
                result = pfRequest.ToString();
                pfErrorHandler.LogText(result, clientId);
                HttpWebRequest request = WebRequest.Create(webServiceUrl) as HttpWebRequest;
                Byte[] bytes = Encoding.UTF8.GetBytes(result);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 1800000;
                request.ReadWriteTimeout = 1800000;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                int responseCount = 0;
                bool checkResponse = true;
                Console.WriteLine("---Get Products From PF---");
                while (checkResponse)
                {
                    try
                    {
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                checkResponse = false;
                                StreamReader reader = new StreamReader(response.GetResponseStream());
                                responseString = reader.ReadToEnd();
                            }
                            else
                            {
                                responseCount += 1;
                                if (responseCount == 3)
                                {
                                    checkResponse = false;
                                }
                            }

                        }
                    }
                    catch (WebException webEx)
                    {
                        if (webEx.Status != WebExceptionStatus.Success)
                        {
                            pfErrorHandler.LogError(webEx, clientId, "0");
                            responseCount += 1;
                            if (responseCount == 3)
                            {
                                checkResponse = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        pfErrorHandler.LogError(ex, clientId, "0");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }
            return responseString;
        }

        //Call PF Export to request products based on page number
        public string RequestProductsByPageNo(int pageNo)
        {
            string result = String.Empty;
            string responseString = string.Empty;
            int clientId = 4003;
            try
            {
                StringBuilder pfRequest = new StringBuilder();
                string webServiceUrl = pfApiObjectExport;
                pfRequest.AppendLine("<export>");
                pfRequest.AppendLine(string.Format("<sec cid=\"{0}\">", clientId));
                pfRequest.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                pfRequest.AppendLine("</sec>");
                pfRequest.AppendLine("<params>");
                pfRequest.AppendLine("<tid>15</tid>");
                pfRequest.AppendLine(string.Format("<pgeno>{0}</pgeno>", pageNo));
                pfRequest.AppendLine("<recspge>25</recspge>");
                pfRequest.AppendLine("<stat>published</stat>");
                pfRequest.AppendLine("</params>");
                pfRequest.AppendLine("<prop>");
                pfRequest.AppendLine("<p id=\"450\" nm=\"product type\" />");
                pfRequest.AppendLine("<p id=\"143\" nm=\"product code\" />");
                pfRequest.AppendLine("<p id=\"3143\" nm=\"ean\" />");
                pfRequest.AppendLine("<p id=\"11001898\" nm=\"tmall id\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"style\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"hero code\" />");
                pfRequest.AppendLine("<p id=\"2730\" nm=\"hero product\" />");
                pfRequest.AppendLine("<p id=\"403\" nm=\"web product name\" />");
                pfRequest.AppendLine("<p id=\"1741\" nm=\"product summary\" />");
                pfRequest.AppendLine("<p id=\"144\" nm=\"product description\" />");
                pfRequest.AppendLine("<p id=\"391\" nm=\"variation\" roids=\"1002099,1002100\" ronms=\"colour,size\" />");
                pfRequest.AppendLine("<p id=\"220\" nm=\"price\" roids=\"249\" ronms=\"retail\" />");
                pfRequest.AppendLine("<p id=\"233\" nm=\"soh\" roids=\"335\" ronms=\"global inventory\"/>");
                pfRequest.AppendLine("<p id=\"447\" nm=\"image1\" />");
                pfRequest.AppendLine("<p id=\"1952\" nm=\"start sale date\" />");
                pfRequest.AppendLine("<p id=\"1953\" nm=\"end sale date\" />");
                pfRequest.AppendLine("<p id=\"161\" nm=\"category\" sw=\"returnid\" />");
                pfRequest.AppendLine("<p id=\"446\" nm=\"image thumbnail\" />");
                pfRequest.AppendLine("<p id=\"1576\" />");
                pfRequest.AppendLine("</prop>");
                pfRequest.AppendLine("</export>");
                result = pfRequest.ToString();
                pfErrorHandler.LogText(result, clientId);
                HttpWebRequest request = WebRequest.Create(webServiceUrl) as HttpWebRequest;
                Byte[] bytes = Encoding.UTF8.GetBytes(result);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 1800000;
                request.ReadWriteTimeout = 1800000;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                int responseCount = 0;
                bool checkResponse = true;
                Console.WriteLine("---Get Products From PF---");
                while (checkResponse)
                {
                    try
                    {
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                checkResponse = false;
                                StreamReader reader = new StreamReader(response.GetResponseStream());
                                responseString = reader.ReadToEnd();
                            }
                            else
                            {
                                responseCount += 1;
                                if (responseCount == 3)
                                {
                                    checkResponse = false;
                                }
                            }
                        }
                    }
                    catch (WebException webEx)
                    {
                        if (webEx.Status != WebExceptionStatus.Success)
                        {
                            pfErrorHandler.LogError(webEx, clientId, "0");
                            responseCount += 1;
                            if (responseCount == 3)
                            {
                                checkResponse = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        pfErrorHandler.LogError(ex, clientId, "0");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }
            return responseString;
        }

        //Generate partner ID
        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        //Call PF Export to retrieve total products with published status
        public string GetTotalPublishedProduct()
        {
            string result = String.Empty;
            string responseString = String.Empty;
            try
            {
                StringBuilder pfRequest = new StringBuilder();
                string webServiceUrl = pfApiObjectExport;
                pfRequest.AppendLine("<export>");
                pfRequest.AppendLine(string.Format("<sec cid=\"{0}\">", clientId));
                pfRequest.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                pfRequest.AppendLine("</sec>");
                pfRequest.AppendLine("<params>");
                pfRequest.AppendLine("<tid>15</tid>");
                pfRequest.AppendLine("<reccount>true</reccount>");
                pfRequest.AppendLine("<stat>published</stat>");
                pfRequest.AppendLine("</params>");
                pfRequest.AppendLine("<prop>");
                pfRequest.AppendLine("<p id=\"450\" nm=\"product type\" />");
                pfRequest.AppendLine("<p id=\"143\" nm=\"product code\" />");
                pfRequest.AppendLine("<p id=\"3143\" nm=\"ean\" />");
                pfRequest.AppendLine("<p id=\"11001898\" nm=\"tmall id\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"style\" />");
                pfRequest.AppendLine("<p id=\"2839\" nm=\"hero code\" />");
                pfRequest.AppendLine("<p id=\"2730\" nm=\"hero product\" />");
                pfRequest.AppendLine("<p id=\"403\" nm=\"web product name\" />");
                pfRequest.AppendLine("<p id=\"1741\" nm=\"product summary\" />");
                pfRequest.AppendLine("<p id=\"144\" nm=\"product description\" />");
                pfRequest.AppendLine("<p id=\"391\" nm=\"variation\" roids=\"1002099,1002100\" ronms=\"colour,size\" />");
                pfRequest.AppendLine("<p id=\"220\" nm=\"price\" roids=\"249\" ronms=\"retail\" />");
                pfRequest.AppendLine("<p id=\"233\" nm=\"soh\" />");
                pfRequest.AppendLine("<p id=\"447\" nm=\"image1\" />");
                pfRequest.AppendLine("<p id=\"1952\" nm=\"start sale date\" />");
                pfRequest.AppendLine("<p id=\"1953\" nm=\"end sale date\" />");
                pfRequest.AppendLine("<p id=\"161\" nm=\"category\" sw=\"returnid\" />");
                pfRequest.AppendLine("<p id=\"446\" nm=\"image thumbnail\" />");
                pfRequest.AppendLine("<p id=\"1576\" />");
                pfRequest.AppendLine("</prop>");
                pfRequest.AppendLine("</export>");
                result = pfRequest.ToString();
                pfErrorHandler.LogText(result, clientId);
                HttpWebRequest request = WebRequest.Create(webServiceUrl) as HttpWebRequest;
                Byte[] bytes = Encoding.UTF8.GetBytes(result);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 1800000;
                request.ReadWriteTimeout = 1800000;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                int responseCount = 0;
                bool checkResponse = true;
                while (checkResponse)
                {
                    try
                    {
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                checkResponse = false;
                                StreamReader reader = new StreamReader(response.GetResponseStream());
                                responseString = reader.ReadToEnd();
                            }
                            else
                            {
                                responseCount += 1;
                                if (responseCount == 3)
                                {
                                    checkResponse = false;
                                }
                            }
                        }
                    }
                    catch (WebException webEx)
                    {
                        if (webEx.Status != WebExceptionStatus.Success)
                        {
                            pfErrorHandler.LogError(webEx, clientId, "0");
                            responseCount += 1;
                            if (responseCount == 3)
                            {
                                checkResponse = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        pfErrorHandler.LogError(ex, clientId, "0");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }
            return responseString;
        }

        //Divide the total products by 25 to return total page number
        public int GetPfStocksCount()
        {
            string totalProductResponse = GetTotalPublishedProduct();
            int stockPages = 0;
            if (!string.IsNullOrEmpty(totalProductResponse))
            {
                XDocument totalProductDoc = XDocument.Parse(totalProductResponse);
                var totalProducts = from product in totalProductDoc.Descendants("resp")
                                    where product.Attribute("stat").Value == "1"
                                    select product.Attribute("recs").Value;
                int productCount = int.Parse(totalProducts.FirstOrDefault());
                stockPages = ((int)Math.Ceiling((double)productCount / (double)25));
            }
            return stockPages;
        }
        //Update product property value after successful add the product
        public bool UpdateProductStatus(string productInfo)
        {
            Console.WriteLine("-------Update Product Status--------");
            string responseString;
            bool checkUpdateStatus = false;
            try
            {
                String webServiceURL = pfApiObjectImport;
                string pfProductId = productInfo.Split(',')[0];
                string tmallProductId = productInfo.Split(',')[1];
                string date = productInfo.Split(',')[2];
                DateTime delist = DateTime.ParseExact(date, "yyyy-MM-dd HH:mm:ss", null);
                HttpWebRequest request = WebRequest.Create(webServiceURL) as HttpWebRequest;
                StringBuilder xmlBuilder = new StringBuilder();
                int clientID = 4003;
                xmlBuilder.AppendLine("<?xml version=\"1.0\"?>");
                xmlBuilder.AppendLine("<import>");
                xmlBuilder.AppendLine(string.Format("<sec cid=\"{0}\" >", clientID));
                xmlBuilder.AppendLine(string.Format("<login usr=\"{0}\" pass=\"{1}\" />", pfApiUsername, pfApiPassword));
                xmlBuilder.AppendLine("</sec>");
                xmlBuilder.AppendLine("<params />");
                xmlBuilder.AppendLine("<obj>");
                xmlBuilder.AppendLine(string.Format("<o tid=\"15\" id=\"{0}\">", pfProductId));
                xmlBuilder.AppendLine("<prop>");
                xmlBuilder.AppendLine(string.Format("<p id=\"11001898\" nm=\"Tmall Id\"><val><v>{0}</v></val></p>", tmallProductId));
                xmlBuilder.AppendLine(string.Format("<p id=\"11001899\" nm=\"Tmall Ending Time\"><val><v>{0}</v></val></p>", delist.ToString("dd/MM/yyyy HH:mm:ss")));
                xmlBuilder.AppendLine("</prop>");
                xmlBuilder.AppendLine("</o>");
                xmlBuilder.AppendLine("</obj>");
                xmlBuilder.AppendLine("</import>");

                string xmlRequest = xmlBuilder.ToString();
                pfErrorHandler.LogText(xmlRequest, clientId);
                Byte[] bytes = Encoding.UTF8.GetBytes(xmlRequest);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                int getResponseTry = 0;
                bool getResponse = true;
                while (getResponse)
                {
                    try
                    {
                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                getResponse = false;
                                StreamReader reader = new StreamReader(response.GetResponseStream());

                                responseString = reader.ReadToEnd();
                                XmlDocument xmlItemDoc = new XmlDocument();
                                xmlItemDoc.LoadXml(responseString);

                                XmlNode varsNode = xmlItemDoc.SelectSingleNode(@"/import/obj/success/o");
                                if (varsNode != null)
                                {
                                    checkUpdateStatus = true;
                                }
                            }
                            else
                            {
                                getResponseTry += 1;
                                if (getResponseTry == 3)
                                {
                                    getResponse = false;
                                }
                            }
                        }
                    }
                    catch (WebException webEx)
                    {
                        if (webEx.Status != WebExceptionStatus.Success)
                        {
                            pfErrorHandler.LogError(webEx, clientId, "0");
                            getResponseTry += 1;
                            if (getResponseTry == 3)
                            {
                                getResponse = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        pfErrorHandler.LogError(ex, clientId, "0");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                pfErrorHandler.LogError(ex, clientId, "0");
                throw ex;
            }

            return checkUpdateStatus;
        }

    }
    #endregion
}

