﻿using PFTMall;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Top.Api;

namespace PFTMall
{
    class PFTmallStockUpload : PFTmallServiceBase
    {
        #region Constructor
        public PFTmallStockUpload(int clientId) : base(clientId)
        {
            processId = "0";
        }
        #endregion

        #region Public Method
        public override void ExecuteTask()
        {
            ExecuteTask(null);
        }

        public override void ExecuteTask(Object o)
        {
            while (runThread)
            {

                this.DatabaseHandler.BeginTransaction();
                try
                {
                    pfApi.LoadClientSettings();
                    tmallApi = new PFTmallAPI();
                    tmallApi.appKey = pfApi.AppKey;
                    tmallApi.appSecret = pfApi.AppSecret;
                    tmallApi.sessionKey = pfApi.SessionKey;
                    tmallApi.client = pfApi.client;
                    tmallApi.pfAPi = pfApi;
                    this.DatabaseHandler.CommitTransaction();
                    uploadStock();
                }
                catch (Exception ex)
                {
                    this.DatabaseHandler.RollBackTransaction();
                    this.errorHandler.LogError(ex, pfApi.clientId, this.processId);
                }
                Thread.Sleep(new TimeSpan(0, 0, 1));
            }
        }
        /// <summary>
        /// Upload stock from PF to Tmall
        /// </summary>
        public void uploadStock()
        {
            string productAddedInfo = "";
            string tmallResponse = "";
            string pfProductId = "";
            string pfProductTmallId = "";
            string productDetails = "";
            int pfProductPages;
            string pfProductXML;
            try
            {
                writeLog("Get Total Pages of Product From PF", serviceName + "-Information");
                //Get total pages of PF Published products
                pfProductPages = pfApi.GetPfStocksCount();
                if (pfProductPages == 0)
                {
                    throw new ArgumentNullException();
                }
                //Export PF product by page number to avoid delay
                for (int pfPage = 1; pfPage <= pfProductPages; pfPage++)
                {
                    writeLog("Request products From PF By Page Number", serviceName + "-Information");
                    pfProductXML = pfApi.RequestProductsByPageNo(pfPage);
                    if (!this.ErrorHandler.LogPFApiResponse(pfProductXML, pfApi.clientId, this.processId))
                    {
                        XDocument pfProductDoc = XDocument.Parse(pfProductXML);
                        if (pfProductDoc.Root.Element("resp").Attribute("stat").Value != "0" && pfProductDoc.Descendants("o").Count() > 0)
                        {
                            var pfProducts = pfProductDoc.Descendants("o");
                            writeLog("Retrieve PF Product ID and Tmall Product ID", serviceName + "-Information");
                            foreach (var pfProduct in pfProducts)
                            {
                                pfProductId = pfProduct.Attribute("id").Value;
                                pfProductTmallId = pfProduct.Descendants()
                                                   .Where(x => (string)x.Attribute("id") == "11001898").FirstOrDefault().Value;
                                writeLog("Check if the product[" + pfProductId + "] has Tmall Product ID to filter duplicate", serviceName + "-Information");
                                //Empty define the product haven't been added to Tmall
                                if (string.IsNullOrEmpty(pfProductTmallId))
                                {
                                    StringBuilder productXml = new StringBuilder();
                                    productXml.AppendLine(pfProduct.ToString());
                                    writeLog("Import Product to Tmall", serviceName + "-Information");
                                    //Call Tmall AddProduct API
                                    tmallResponse = pfTmallApi.AddItem(tmallApi.client, tmallApi.sessionKey
                                        , productXml.ToString());
                                    writeLog("Check if import product successful", serviceName + "-Information");
                                    if (!string.IsNullOrEmpty(tmallResponse))
                                    {
                                        //Process to response to get Tmall Product ID and Delist time to be updated in PF
                                        productAddedInfo = processTmallResponse(tmallResponse, pfProductId);
                                        if (!string.IsNullOrEmpty(productAddedInfo))
                                        {
                                            productDetails = pfProductId + "," + productAddedInfo;
                                            if (pfApi.UpdateProductStatus(productDetails))
                                            {
                                                writeLog("Product(" + pfProductId + ") Tmall Details Update Successful", serviceName + "-Information");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        errorHandler.LogError(new Exception("Error while import product to tMall"), pfApi.clientId, pfProductId);
                                    }
                                }
                                else
                                {
                                    writeLog("Product[" + pfProductId + "] has existed in Tmall", serviceName + "-Information");
                                }
                            }
                        }
                    }
                    else
                    {
                        errorHandler.LogError(new Exception("Error while trying to export data from Powerfront API"),
                        pfApi.clientId, "0");
                    }
                }

            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex, pfApi.clientId, "0");
            }
        }

        //Process the response from addProduct
        public string processTmallResponse(string tmallResponse, string pfProductId)
        {
            string itemResponse = "";
            string response = "";
            try
            {
                writeLog("Process AddItemResponse for Product[" + pfProductId + "]", serviceName + "-Information");
                XDocument doc = XDocument.Parse(tmallResponse);
                //Retrieve tmall product id
                var tmallProduct = doc.Descendants("item").Select(s => new
                {
                    ID = s.Element("num_iid").Value
                });
                writeLog("Request delist time for Product[" + pfProductId + "]", serviceName + "-Information");
                //Call GetItem API to get detail of the item such as delist time
                itemResponse = tmallApi.GetItemInfo(tmallApi.client, tmallApi.sessionKey, long.Parse(tmallProduct.FirstOrDefault().ID), pfProductId);
                if (!string.IsNullOrEmpty(itemResponse))
                {
                    XDocument itemDoc = XDocument.Parse(itemResponse);
                    var delistTime = itemDoc.Descendants("item").Select(s => new
                    {
                        Delist = s.Element("delist_time").Value
                    });
                    response = tmallProduct.FirstOrDefault().ID.ToString() + "," + delistTime.FirstOrDefault().Delist.ToString();
                }

            }
            catch (Exception ex)
            {
                errorHandler.LogError(ex, pfApi.clientId, "0");
            }

            return response;
        }
    }
    #endregion
}
