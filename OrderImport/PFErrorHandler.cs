﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PFTMall
{
    class PFErrorHandler
    {
        public PFTmallServiceBase pfTmallServiceBase { get; set; }

        public void LogError(Exception ex, int clientID, string productID)
        {
            StringBuilder errorDescription = new StringBuilder();
            errorDescription.AppendLine("Source: " + ex.Source);
            errorDescription.AppendLine("Message: " + ex.Message);
            errorDescription.AppendLine("StackTrace: " + ex.StackTrace);

            short errorType = -1;
            int errorCode = -1;
            string errorMessage = ex.ToString();
            string errorTimestamp = DateTime.Now.ToString();

            if (pfTmallServiceBase != null)
            {
                pfTmallServiceBase.writeLog(errorMessage, "Error");
            }
            PFDatabaseHandler pfDatabaseHandler = new PFDatabaseHandler();
            pfDatabaseHandler.BeginTransaction();
            pfDatabaseHandler.InsertErrorLog(clientID, errorType, errorCode, errorMessage, productID, errorTimestamp);
            pfDatabaseHandler.CommitTransaction();
        }

        public void LogText(string text, int clientId)
        {
            StringBuilder errorDescription = new StringBuilder();
            errorDescription.AppendLine("Request: " + text);
            short errorType = -2;
            int errorCode = -2;
            string errorMessage = errorDescription.ToString();
            string errorTimestamp = DateTime.Now.ToString();
            if (pfTmallServiceBase != null)
            {
                pfTmallServiceBase.writeLog(errorMessage, "Information");
            }
            PFDatabaseHandler dbHandler = new PFDatabaseHandler();
            dbHandler.BeginTransaction();
            dbHandler.InsertErrorLog(clientId, errorType, errorCode, errorMessage, "0", errorTimestamp);
            dbHandler.CommitTransaction();
        }
        public bool LogPFApiResponse(string response, int clientID, string productID)
        {
            PFDatabaseHandler pfDatabaseHandler;

            if (response.Length == 0)
            {
                pfDatabaseHandler = new PFDatabaseHandler();
                pfDatabaseHandler.BeginTransaction();
                pfDatabaseHandler.InsertErrorLog(clientID, -1, -1, "Response is empty, HttpResponse from PFAPI is not ok or there's an webexception: " + response,
                    productID, DateTime.Now.ToString());
                pfDatabaseHandler.CommitTransaction();

                return true; 
            }

            XmlDocument responseDoc = new XmlDocument();
            responseDoc.LoadXml(response);

            bool isError = false;
            XmlNodeList exportErrorNodes = responseDoc.SelectNodes(@"/export/err");
            XmlNodeList importErrorNodes = responseDoc.SelectNodes(@"/import/err");
            XmlNodeList transactionErrorNodes = responseDoc.SelectNodes(@"/transactions/err");

            pfDatabaseHandler = new PFDatabaseHandler();
            pfDatabaseHandler.BeginTransaction();

            if (exportErrorNodes != null && exportErrorNodes.Count > 0)
            {
                short errorType = 2; 
                int errorCode = -1;
                string errorMessage = response;
                string errorTimestamp = DateTime.Now.ToString();

                if (pfTmallServiceBase != null)
                {
                    pfTmallServiceBase.writeLog(errorMessage, "Error");
                }
                pfDatabaseHandler.InsertErrorLog(clientID, errorType, errorCode, errorMessage, productID, errorTimestamp);
                isError = true;
            }

            if (importErrorNodes != null && importErrorNodes.Count > 0)
            {
                short errorType = 3; 
                int errorCode = -1;
                string errorMessage = response;
                string errorTimestamp = DateTime.Now.ToString();


                if (pfTmallServiceBase != null)
                {
                    pfTmallServiceBase.writeLog(errorMessage, "Error");
                }

                pfDatabaseHandler.InsertErrorLog(clientID, errorType, errorCode, errorMessage, productID, errorTimestamp);
                isError = true;
            }

            if (transactionErrorNodes != null && transactionErrorNodes.Count > 0)
            {
                short errorType = 4; 
                int errorCode = -1;
                string errorMessage = response;
                string errorTimestamp = DateTime.Now.ToString();


                if (pfTmallServiceBase != null)
                {
                    pfTmallServiceBase.writeLog(errorMessage, "Error");
                }

                pfDatabaseHandler.InsertErrorLog(clientID, errorType, errorCode, errorMessage, productID, errorTimestamp);
                isError = true;
            }

            transactionErrorNodes = responseDoc.SelectNodes(@"/transactions/obj/errs");
            if (transactionErrorNodes != null && transactionErrorNodes.Count > 0)
            {
                StringBuilder errorDescription = new StringBuilder();

                foreach (XmlNode objNode in transactionErrorNodes[0].SelectNodes(@"./o"))
                {
                    foreach (XmlNode errorNode in objNode.SelectNodes(@"./oerrs/err"))
                    {
                        errorDescription.AppendLine(errorNode.Attributes["msg"].Value);
                    }
                }

                short errorType = 4; 
                int errorCode = -1;
                string errorMessage = errorDescription.ToString();
                string errorTimestamp = DateTime.Now.ToString();

                if (errorMessage.Length > 0)
                {

                    if (pfTmallServiceBase != null)
                    {
                        pfTmallServiceBase.writeLog(response, "Error");
                    }
                    pfDatabaseHandler.InsertErrorLog(clientID, errorType, errorCode, response, productID, errorTimestamp);
                    isError = true;
                }
            }

            pfDatabaseHandler.CommitTransaction();

            return isError;
        }

    }
}
