﻿using System;
using System.Data;
using System.Threading;
using System.Xml;
using Top.Api;

namespace PFTMall
{
    class PFTmallOrderImport : PFTmallServiceBase
    {
        #region Constructor
        public PFTmallOrderImport(int clientId) : base(clientId)
        {
            processId = "0";
        }
        #endregion

        public override void ExecuteTask()
        {
            ExecuteTask(null);
        }

        #region Public Method
        public override void ExecuteTask(Object o)
        {
            while (runThread)
            {
                this.DatabaseHandler.BeginTransaction();
                try
                {
                    pfApi.LoadClientSettings();
                    tmallApi = new PFTmallAPI();
                    tmallApi.appKey = pfApi.AppKey;
                    tmallApi.appSecret = pfApi.AppSecret;
                    tmallApi.sessionKey = pfApi.SessionKey;
                    tmallApi.client = pfApi.client;
                    tmallApi.pfAPi = pfApi;
                    pfApi.LoadClientOrderData();
                    this.DatabaseHandler.CommitTransaction();
                    OrderImport();
                }
                catch (Exception ex)
                {
                    this.DatabaseHandler.RollBackTransaction();
                    this.ErrorHandler.LogError(ex, pfApi.clientId, this.processId);
                }
                Thread.Sleep(new TimeSpan(0, 0, 2));
            }
        }

        private void OrderImport()
        {
            string orderResponse;
            string pfImportResponse;
            XmlDocument orderDoc = new XmlDocument();
            //Get the order data from database
            DataView orderView = pfApi.SingleProductData.DefaultView;
            if (orderView.Count > 0)
            {
                foreach (DataRowView row in orderView)
                {
                    //Get order detail by calling tmall API with orderid
                    orderResponse = pfTmallApi.GetOrderFullDetails(tmallApi.client, tmallApi.sessionKey, row["orderid"].ToString());
                    if (pfTmallApi.ExceptionList.Count > 0)
                    {
                        foreach (Exception ex in pfTmallApi.ExceptionList)
                        {
                            this.errorHandler.LogError(ex, pfApi.clientId, "0");
                        }
                        continue;
                    }
                    try
                    {
                        orderDoc.LoadXml(orderResponse);
                    }
                    catch (Exception ex)
                    {
                        writeLog("Exception raised when trying to load order response from tMall " + ex.Message, serviceName+"-Error");
                        return;
                    }
                    try
                    {
                        pfImportResponse = pfApiCall.ImportOrder(orderDoc);
                        if (!this.ErrorHandler.LogPFApiResponse(pfImportResponse, pfApi.clientId, this.processId))
                        {
                            writeLog("Import Success", serviceName + "-Information");
                        }
                    }
                    catch (Exception ex)
                    {
                        writeLog("Exception raised when trying to import order to PF " + ex.Message, serviceName+"-Error");
                        return;
                    }
                }
            }
        }
        #endregion
    }
}
