﻿using System.Threading;
using System.Configuration;
using System.IO;
using System;

namespace PFTMall
{
    class Program
    {
        //Update session key with refresh token before start 
        static void Main(string[] args)
        {
            int clientId = 4003;
            PFTmallAPI pfTmallApi = new PFTmallAPI();
            pfTmallApi.UpdateToken(ConfigurationManager.AppSettings["RefreshToken"]);
            StartServices(clientId);
        }

        private static void StartServices(int clientId)
        {
            Thread serviceThread;

            //Load orders from Tmall
            PFTmallOrderLoader pfTmallOrderLoader = new PFTmallOrderLoader(clientId);
            pfTmallOrderLoader.serviceName = "Order Loader";
            serviceThread = new Thread(new ThreadStart(pfTmallOrderLoader.ExecuteTask));
            serviceThread.IsBackground = true;
            serviceThread.Start();

            //Import order to PF
            PFTmallOrderImport pfTmallOrderImport = new PFTmallOrderImport(clientId);
            pfTmallOrderImport.serviceName = "Order Import";
            serviceThread = new Thread(new ThreadStart(pfTmallOrderImport.ExecuteTask));
            serviceThread.IsBackground = true;
            serviceThread.Start();

            //Upload Stock
            PFTmallStockUpload pfTmallStockUpload = new PFTmallStockUpload(clientId);
            pfTmallStockUpload.serviceName = "Stock Upload";
            serviceThread = new Thread(new ThreadStart(pfTmallStockUpload.ExecuteTask));
            serviceThread.IsBackground = true;
            serviceThread.Start();

            //Update Stock
            PFTmallStockUpdate pfTmallStockUpdate = new PFTmallStockUpdate();
            serviceThread = new Thread(new ThreadStart(pfTmallStockUpdate.ExecuteTask));
            serviceThread.IsBackground = true;
            serviceThread.Start();

            Console.ReadLine();
        }


        private static void WriteLog(string message, string errorType)
        {
            FileStream fileStream;
            string logFilePath = ConfigurationManager.AppSettings["LogFileLocation"];
            if (logFilePath != null)
            {
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }
                fileStream = new FileStream(string.Format("{0}{1}{2}.txt",
                    logFilePath, "PFTmallIntegrationLog", DateTime.Now.ToString("yyyyMMdd")),
                    FileMode.OpenOrCreate, FileAccess.Write);
            }
            else
            {
                fileStream = new FileStream(string.Format(Directory.GetCurrentDirectory() + "\\PFTmallIntegrationLog{0}.txt",
                DateTime.Now.ToString("yyyyMMdd")),
                FileMode.OpenOrCreate, FileAccess.Write);
            }
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            streamWriter.WriteLine(string.Format("Log {0} ({1}): {2} \n", errorType,
                DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"), message));
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
